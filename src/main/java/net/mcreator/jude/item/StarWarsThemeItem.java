
package net.mcreator.jude.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.Rarity;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;

import net.mcreator.jude.JudeModElements;

@JudeModElements.ModElement.Tag
public class StarWarsThemeItem extends JudeModElements.ModElement {
	@ObjectHolder("jude:star_wars_theme")
	public static final Item block = null;
	public StarWarsThemeItem(JudeModElements instance) {
		super(instance, 52);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new MusicDiscItemCustom());
	}
	public static class MusicDiscItemCustom extends MusicDiscItem {
		public MusicDiscItemCustom() {
			super(0, JudeModElements.sounds.get(new ResourceLocation("jude:star_wars_theme")),
					new Item.Properties().group(ItemGroup.MISC).maxStackSize(1).rarity(Rarity.RARE));
			setRegistryName("star_wars_theme");
		}
	}
}
