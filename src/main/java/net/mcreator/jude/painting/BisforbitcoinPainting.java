
package net.mcreator.jude.painting;

import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.entity.item.PaintingType;

import net.mcreator.jude.JudeModElements;

@JudeModElements.ModElement.Tag
public class BisforbitcoinPainting extends JudeModElements.ModElement {
	public BisforbitcoinPainting(JudeModElements instance) {
		super(instance, 30);
		FMLJavaModLoadingContext.get().getModEventBus().register(this);
	}

	@SubscribeEvent
	public void registerPaintingType(RegistryEvent.Register<PaintingType> event) {
		event.getRegistry().register(new PaintingType(16, 32).setRegistryName("bisforbitcoin"));
	}
}
