package net.mcreator.jude.procedures;

import net.minecraft.potion.Effects;
import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.jude.JudeModElements;
import net.mcreator.jude.JudeMod;

import java.util.Map;

@JudeModElements.ModElement.Tag
public class PurplelightsaberLivingEntityIsHitWithToolProcedure extends JudeModElements.ModElement {
	public PurplelightsaberLivingEntityIsHitWithToolProcedure(JudeModElements instance) {
		super(instance, 21);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				JudeMod.LOGGER.warn("Failed to load dependency entity for procedure PurplelightsaberLivingEntityIsHitWithTool!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.INSTANT_DAMAGE, (int) 60, (int) 1));
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.WEAKNESS, (int) 60, (int) 1));
	}
}
